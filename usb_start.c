/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */
#include "usb_start.h"

static uint8_t desc_bytes[] =
  { DEV_DESC, CFG_DESC, IFACE_DESC };
static struct usbd_descriptors desc[] =
  {
    { desc_bytes, desc_bytes + sizeof(desc_bytes) } };

static uint8_t ctrl_buffer[64];
static uint8_t buff[64];

static bool
usb_device_cb_bulk_in (const uint8_t ep, const enum usb_xfer_code rc,
                       const uint32_t count)
{
// just send it and wait for more data
  memset (buff, 0, 64);
  return usbdc_xfer (CONF_USB_CCID_BULKOUT_EPADDR, buff, 64, false);
}

static bool
usb_device_cb_bulk_out (const uint8_t ep, const enum usb_xfer_code rc,
                        const uint32_t count)
{
  int len;
  if (ccid_do (buff, &len))
    {
      memset (buff, 0, 64);
      return false;
    }
  // zero out the rest of the buffer
  memset (&buff[len], 0, 64 - len);
  return usbdc_xfer (CONF_USB_CCID_BULKIN_EPADDR, buff, len, len == 64);
}

struct ccidf_func_data
{
  bool enabled;
};
static struct ccidf_func_data _ciddf_funcd;

int32_t
cciddf_enable (struct usbdf_driver *drv, enum usbdf_control ctrl, void *param)
{
  int rc;
  switch (ctrl)
    {
    case USBDF_GET_IFACE:
      return ERR_UNSUPPORTED_OP;
    case USBDF_DISABLE:
      usb_d_ep_deinit (CONF_USB_CCID_BULKOUT_EPADDR);

      usb_d_ep_deinit (CONF_USB_CCID_BULKIN_EPADDR);
      _ciddf_funcd.enabled = false;
      return ERR_NONE;
    case USBDF_ENABLE:
      if (_ciddf_funcd.enabled)
        {
          // why are we enabling twice?
          return ERR_NOT_INITIALIZED;
        }
      rc = usb_d_ep_init (CONF_USB_CCID_BULKOUT_EPADDR, USB_EP_TYPE_BULK,
      CONF_USB_CCID_BULKOUT_MAXPKSZ);
      if (rc < 0)
        {
          return ERR_NOT_INITIALIZED;
        }

      rc = usb_d_ep_init (CONF_USB_CCID_BULKIN_EPADDR, USB_EP_TYPE_BULK,
      CONF_USB_CCID_BULKIN_MAXPKSZ);
      if (rc < 0)
        {
          return ERR_NOT_INITIALIZED;
        }

      rc = usb_d_ep_enable (CONF_USB_CCID_BULKOUT_EPADDR);
      if (rc < 0)
        {
          return ERR_NOT_INITIALIZED;
        }

      rc = usb_d_ep_enable (CONF_USB_CCID_BULKIN_EPADDR);
      if (rc < 0)
        {
          return ERR_NOT_INITIALIZED;
        }

      usb_d_ep_register_callback (CONF_USB_CCID_BULKOUT_EPADDR,
                                  USB_D_EP_CB_XFER,
                                  (FUNC_PTR) usb_device_cb_bulk_out);

      usb_d_ep_register_callback (CONF_USB_CCID_BULKIN_EPADDR, USB_D_EP_CB_XFER,
                                  (FUNC_PTR) usb_device_cb_bulk_in);

      _ciddf_funcd.enabled = true;

      return ERR_NONE;
    }
  return ERR_INVALID_ARG;
}

// processes the CLASS request
static int32_t
req (uint8_t ep, struct usb_req *req, enum usb_ctrl_stage stage)
{
  // ERR_NOT_FOUND means the "base" usbdc handler will take on the request
  // we shouldn't bother implementing get/set descriptors
  // This is checking that D7=1 (Device to host) and D6=1 (Class)
  // if it's not that, fall through to the "base" handler
  if (0x01 != ((req->bmRequestType >> 5) & 0x03))
    {
      return ERR_NOT_FOUND;
    }

  if (req->bRequest == 0x01)
    {
      // ABORT
      return ERR_NONE;
    }
  else if (req->bRequest == 0x02)
    {
      // GET_CLOCK_FREQUENCIES
      return ERR_NONE;
    }
  else if (req->bRequest == 0x03)
    {
      // GET_DATA_RATES
      return ERR_NONE;

    }

  return ERR_INVALID_ARG;
}

static struct usbdf_driver driver;

static struct usbdc_handler req_h =
  { NULL, (FUNC_PTR) req };

int32_t
cciddf_init (void)
{
  if (usbdc_get_state () > USBD_S_POWER)
    {
      return ERR_DENIED;
    }

  driver.ctrl = cciddf_enable;
  driver.func_data = &_ciddf_funcd;
  usbdc_register_function (&driver);
  usbdc_register_handler (USBDC_HDL_REQ, &req_h);

  return ERR_NONE;
}

void
usb_init (void)
{
  usbdc_init (ctrl_buffer);

  cciddf_init ();
}

int
usb_start (void)
{
  usbdc_start (desc);
  usbdc_attach ();

  while (!_ciddf_funcd.enabled)
    {
      // wait...
    }
  // read data
  return ccid_start (buff);
}
