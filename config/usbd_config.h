/* Auto-generated config file usbd_config.h */
#ifndef USBD_CONFIG_H
#define USBD_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// ---- USB Device Stack Core Options ----

// <q> High Speed Support
// <i> Enable high speed specific descriptors support, e.g., DeviceQualifierDescriptor and OtherSpeedConfiguration Descriptor.
// <i> High speed support require descriptors description array on start, for LS/FS and HS support in first and second place.
// <id> usbd_hs_sp
#ifndef CONF_USBD_HS_SP
#define CONF_USBD_HS_SP 0
#endif

// ---- USB Device Stack CDC ACM Options ----

// <e> Enable String Descriptors
// <id> usb_cdcd_acm_str_en
#ifndef CONF_USB_CCID_STR_EN
#define CONF_USB_CCID_STR_EN 0
#endif
// <s> Language IDs
// <i> Language IDs in c format, split by comma (E.g., 0x0409 ...)
// <id> usb_cdcd_acm_langid
#ifndef CONF_USB_CCID_LANGID
#define CONF_USB_CCID_LANGID "0x0409"
#endif

#ifndef CONF_USB_CCID_LANGID_DESC
#define CONF_USB_CCID_LANGID_DESC
#endif
// </e>

// <h> CCID Device Descriptor


// <o> bcdUSB
// <0x0200=> USB 2.0 version
// <0x0210=> USB 2.1 version
// <id> usb_cdcd_acm_bcdusb
#ifndef CONF_USB_CCID_BCDUSB
#define CONF_USB_CCID_BCDUSB 0x200
#endif


// <o> bcdDeviceClass
#ifndef CONF_USB_CCID_BDEVICECLASS
#define CONF_USB_CCID_BDEVICECLASS 0x00
#endif

// <o> bDeviceSubclass
#ifndef CONF_USB_CCID_BDEVICESUBCLASS
#define CONF_USB_CCID_BDEVICESUBCLASS 0x00
#endif

#ifndef CONF_USB_CCID_BDEVICEPROTOCOL
#define CONF_USB_CCID_BDEVICEPROTOCOL 0x00
#endif

// <o> bMaxPackeSize0
// <0x0008=> 8 bytes
// <0x0010=> 16 bytes
// <0x0020=> 32 bytes
// <0x0040=> 64 bytes
// <id> usb_cdcd_acm_bmaxpksz0
#ifndef CONF_USB_CCID_BMAXPKSZ0
#define CONF_USB_CCID_BMAXPKSZ0 0x40
#endif

// <o> idVender <0x0000-0xFFFF>
// <id> usb_cdcd_acm_idvender
#ifndef CONF_USB_CCID_IDVENDER
#define CONF_USB_CCID_IDVENDER 0x3eb
#endif

// <o> idProduct <0x0000-0xFFFF>
// <id> usb_cdcd_acm_idproduct
#ifndef CONF_USB_CCID_IDPRODUCT
#define CONF_USB_CCID_IDPRODUCT 0x2404
#endif

// <o> bcdDevice <0x0000-0xFFFF>
// <id> usb_cdcd_acm_bcddevice
#ifndef CONF_USB_CCID_BCDDEVICE
#define CONF_USB_CCID_BCDDEVICE 0x100
#endif

// <e> Enable string descriptor of iManufact
// <id> usb_cdcd_acm_imanufact_en
#ifndef CONF_USB_CCID_IMANUFACT_EN
#define CONF_USB_CCID_IMANUFACT_EN 0
#endif

#ifndef CONF_USB_CCID_IMANUFACT
#define CONF_USB_CCID_IMANUFACT (CONF_USB_CCID_IMANUFACT_EN * (CONF_USB_CCID_IMANUFACT_EN))
#endif

// <s> Unicode string of iManufact
// <id> usb_cdcd_acm_imanufact_str
#ifndef CONF_USB_CCID_IMANUFACT_STR
#define CONF_USB_CCID_IMANUFACT_STR "Atmel"
#endif

#ifndef CONF_USB_CCID_IMANUFACT_STR_DESC
#define CONF_USB_CCID_IMANUFACT_STR_DESC
#endif

// </e>

// <e> Enable string descriptor of iProduct
// <id> usb_cdcd_acm_iproduct_en
#ifndef CONF_USB_CCID_IPRODUCT_EN
#define CONF_USB_CCID_IPRODUCT_EN 0
#endif

#ifndef CONF_USB_CCID_IPRODUCT
#define CONF_USB_CCID_IPRODUCT                                                                                     \
	(CONF_USB_CCID_IPRODUCT_EN * (CONF_USB_CCID_IMANUFACT_EN + CONF_USB_CCID_IPRODUCT_EN))
#endif

// <s> Unicode string of iProduct
// <id> usb_cdcd_acm_iproduct_str
#ifndef CONF_USB_CCID_IPRODUCT_STR
#define CONF_USB_CCID_IPRODUCT_STR "CDC ACM Serial Bridge Demo"
#endif

#ifndef CONF_USB_CCID_IPRODUCT_STR_DESC
#define CONF_USB_CCID_IPRODUCT_STR_DESC
#endif

// </e>

// <e> Enable string descriptor of iSerialNum
// <id> usb_cdcd_acm_iserialnum_en
#ifndef CONF_USB_CCID_ISERIALNUM_EN
#define CONF_USB_CCID_ISERIALNUM_EN 0
#endif

#ifndef CONF_USB_CCID_ISERIALNUM
#define CONF_USB_CCID_ISERIALNUM                                                                                   \
	(CONF_USB_CCID_ISERIALNUM_EN                                                                                   \
	 * (CONF_USB_CCID_IMANUFACT_EN + CONF_USB_CCID_IPRODUCT_EN + CONF_USB_CCID_ISERIALNUM_EN))
#endif

// <s> Unicode string of iSerialNum
// <id> usb_cdcd_acm_iserialnum_str
#ifndef CONF_USB_CCID_ISERIALNUM_STR
#define CONF_USB_CCID_ISERIALNUM_STR "123456789ABCDEF"
#endif

#ifndef CONF_USB_CCID_ISERIALNUM_STR_DESC
#define CONF_USB_CCID_ISERIALNUM_STR_DESC
#endif

// </e>

// <o> bNumConfigurations <0x01-0xFF>
// <id> usb_cdcd_acm_bnumconfig
#ifndef CONF_USB_CCID_BNUMCONFIG
#define CONF_USB_CCID_BNUMCONFIG 0x1
#endif

// </h>

// <h> CDC ACM Configuration Descriptor
// <o> bConfigurationValue <0x01-0xFF>
// <id> usb_cdcd_acm_bconfigval
#ifndef CONF_USB_CCID_BCONFIGVAL
#define CONF_USB_CCID_BCONFIGVAL 0x1
#endif
// <e> Enable string descriptor of iConfig
// <id> usb_cdcd_acm_iconfig_en
#ifndef CONF_USB_CCID_ICONFIG_EN
#define CONF_USB_CCID_ICONFIG_EN 0
#endif

#ifndef CONF_USB_CCID_ICONFIG
#define CONF_USB_CCID_ICONFIG                                                                                      \
	(CONF_USB_CCID_ICONFIG_EN                                                                                      \
	 * (CONF_USB_CCID_IMANUFACT_EN + CONF_USB_CCID_IPRODUCT_EN + CONF_USB_CCID_ISERIALNUM_EN               \
	    + CONF_USB_CCID_ICONFIG_EN))
#endif

// <s> Unicode string of iConfig
// <id> usb_cdcd_acm_iconfig_str
#ifndef CONF_USB_CCID_ICONFIG_STR
#define CONF_USB_CCID_ICONFIG_STR
#endif

#ifndef CONF_USB_CCID_ICONFIG_STR_DESC
#define CONF_USB_CCID_ICONFIG_STR_DESC
#endif

// </e>

// <o> bmAttributes
// <0x80=> Bus power supply, not support for remote wakeup
// <0xA0=> Bus power supply, support for remote wakeup
// <0xC0=> Self powered, not support for remote wakeup
// <0xE0=> Self powered, support for remote wakeup
// <id> usb_cdcd_acm_bmattri
#ifndef CONF_USB_CCID_BMATTRI
#define CONF_USB_CCID_BMATTRI 0x80
#endif

// <o> bMaxPower <0x00-0xFF>
// <id> usb_cdcd_acm_bmaxpower
#ifndef CONF_USB_CCID_BMAXPOWER
#define CONF_USB_CCID_BMAXPOWER 0x32
#endif
// </h>

// <h> CDC ACM Communication Interface Descriptor

// <o> bInterfaceNumber <0x00-0xFF>
// <id> usb_cdcd_acm_comm_bifcnum
#ifndef CONF_USB_CCID_BIFCNUM
#define CONF_USB_CCID_BIFCNUM 0x00
#endif
// <o> bAlternateSetting <0x00-0xFF>
// <id> usb_cdcd_acm_comm_baltset
#ifndef CONF_USB_CCID_BALTSET
#define CONF_USB_CCID_BALTSET 0x00
#endif

// <o> iInterface <0x00-0xFF>
// <id> usb_cdcd_acm_comm_iifc
#ifndef CONF_USB_CCID_IIFC
#define CONF_USB_CCID_IIFC 0x00
#endif

#ifndef CONF_USB_CCID_BULKOUT_EPADDR
#define CONF_USB_CCID_BULKOUT_EPADDR 0x1
#endif

#ifndef CONF_USB_CCID_BULKOUT_MAXPKSZ
#define CONF_USB_CCID_BULKOUT_MAXPKSZ 0x0040
#endif

#ifndef CONF_USB_CCID_BULKIN_EPADDR
#define CONF_USB_CCID_BULKIN_EPADDR 0x81
#endif

#ifndef CONF_USB_CCID_BULKIN_MAXPKSZ
#define CONF_USB_CCID_BULKIN_MAXPKSZ 0x0040
#endif

// <o> Interrupt IN Endpoint Address
// <0x81=> EndpointAddress = 0x81
// <0x82=> EndpointAddress = 0x82
// <0x83=> EndpointAddress = 0x83
// <0x84=> EndpointAddress = 0x84
// <0x85=> EndpointAddress = 0x85
// <0x86=> EndpointAddress = 0x86
// <0x87=> EndpointAddress = 0x87
// <id> usb_cdcd_acm_epaddr
#ifndef CONF_USB_CCID_INT_EPADDR
#define CONF_USB_CCID_INT_EPADDR 0x82
#endif

// <o> Interrupt IN Endpoint wMaxPacketSize
// <0x0008=> 8 bytes
// <0x0010=> 16 bytes
// <0x0020=> 32 bytes
// <0x0040=> 64 bytes
// <id> usb_cdcd_acm_comm_int_maxpksz
// using 0 led to: usbfs: usb_submit_urb returned -90
// so use a small number since we're never actually going to use this packet
#ifndef CONF_USB_CCID_INT_MAXPKSZ
#define CONF_USB_CCID_INT_MAXPKSZ 0x0008
#endif

// <o> Interrupt IN Endpoint Interval <0x00-0xFF>
// <id> usb_cdcd_acm_comm_int_interval
#ifndef CONF_USB_CCID_INT_INTERVAL
#define CONF_USB_CCID_INT_INTERVAL 0xFF
#endif


#endif // USBD_CONFIG_H
