#include "ccid.h"

static int poweredOn = 0x01;

int
ccid_start (uint8_t *buf)
{
  // read PC_to_RDR_GetSlotStatus
  return usbdc_xfer (CONF_USB_CCID_BULKOUT_EPADDR, buf, 16, false);
}

int
ccid_do (uint8_t *buf, int *len)
{
  struct CCIDMessage message = ccid_message (buf);
  struct CCIDMessage response;
  static uint8_t msb[3];
  response.msgSpecificBytes = msb;

  response.bSlot = message.bSlot;
  response.bSeq = message.bSeq;

  switch (message.bMessageType)
    {
    case PC_TO_RDR_ICCPOWEROFF:
      poweredOn = 0x01; // @suppress("No break at end of case")
    case PC_TO_RDR_GETSLOTSTATUS:
      response.bMessageType = RDR_TO_PC_GETSLOTSTATUS;
      response.dwLength = 0;
      response.msgSpecificBytes[0] = poweredOn;
      break;
    case PC_TO_RDR_ICCPOWERON:
      poweredOn = 0x00;
      response.bMessageType = RDR_TO_PC_DATABLOCK;
      response.dwLength = sizeof ATR;
      response.data = ATR;
      break;
    case PC_TO_RDR_XFRBLOCK:
      // TODO move into iso7816
      switch (message.data[1])
        {
        case GET_DATA:
          if (message.data[2] == 0x5F && message.data[3] == 0x52)
            {
              response.bMessageType = RDR_TO_PC_DATABLOCK;
              response.dwLength = sizeof HISTORY_BYTES;
              response.data = HISTORY_BYTES;
            }
          else if (message.data[2] == 0x00 && message.data[3] == 0x4F)
            {
              response.bMessageType = RDR_TO_PC_DATABLOCK;
              response.dwLength = sizeof AID;
              response.data = AID;
            }
          else
            {
              response.bMessageType = RDR_TO_PC_DATABLOCK;
              response.dwLength = sizeof DATA_NOT_FOUND;
              response.data = DATA_NOT_FOUND;
            }
          break;
        case SELECT_FILE:
          if (message.data[4] == 0x06 && message.data[5] == 0xD2
              && message.data[6] == 0x76 && message.data[7] == 0x00
              && message.data[8] == 0x01 && message.data[9] == 0x24
              && message.data[10] == 0x01)
            {
              response.bMessageType = RDR_TO_PC_DATABLOCK;
              response.dwLength = sizeof EVERYTHING_OK;
              // the way scdaemon/openpgp use this no data is actually necessary
              // it's obviously very buggy to not return any data, but... v2
              response.data = EVERYTHING_OK;
            }
          else
            {
              response.bMessageType = RDR_TO_PC_DATABLOCK;
              response.dwLength = sizeof FILE_NOT_FOUND;
              response.data = FILE_NOT_FOUND;
            }
          break;
        default:
          return 1;
        }
      break;
    default:
      return 1;
    }
  *len = response.dwLength + 10;

  buf[0] = response.bMessageType;
  memcpy (&buf[1], &response.dwLength, sizeof(response.dwLength));
  buf[5] = response.bSlot;
  buf[6] = response.bSeq;
  memcpy (&buf[7], response.msgSpecificBytes, 3);
  memcpy (&buf[10], response.data, response.dwLength);

  return 0;
}

struct CCIDMessage
ccid_message (uint8_t *buf)
{
  struct CCIDMessage req;
  req.bMessageType = buf[0];
  // bytes 1-5 are an int32, so use them all here with memcpy
  memcpy (&req.dwLength, &buf[1], 4);
  req.bSlot = buf[5];
  req.bSeq = buf[6];
  req.msgSpecificBytes = &buf[7];
  req.data = &buf[10];

  return req;
}
